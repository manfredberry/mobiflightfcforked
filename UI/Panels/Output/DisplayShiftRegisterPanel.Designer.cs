﻿
namespace MobiFlight.UI.Panels
{
    partial class DisplayShiftRegisterPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.registerLabel = new System.Windows.Forms.Label();
            this.shiftRegistersComboBox = new System.Windows.Forms.ComboBox();
            this.outputPanelLabel = new System.Windows.Forms.Label();
            this.registerOutputPinComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // registerLabel
            // 
            this.registerLabel.AutoSize = true;
            this.registerLabel.Location = new System.Drawing.Point(19, 10);
            this.registerLabel.Name = "registerLabel";
            this.registerLabel.Size = new System.Drawing.Size(93, 17);
            this.registerLabel.TabIndex = 0;
            this.registerLabel.Text = "Shift Register";
            // 
            // shiftRegistersComboBox
            // 
            this.shiftRegistersComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.shiftRegistersComboBox.FormattingEnabled = true;
            this.shiftRegistersComboBox.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "A",
            "B",
            "C",
            "D",
            "E",
            "F"});
            this.shiftRegistersComboBox.Location = new System.Drawing.Point(119, 7);
            this.shiftRegistersComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.shiftRegistersComboBox.MaximumSize = new System.Drawing.Size(177, 0);
            this.shiftRegistersComboBox.MinimumSize = new System.Drawing.Size(45, 0);
            this.shiftRegistersComboBox.Name = "shiftRegistersComboBox";
            this.shiftRegistersComboBox.Size = new System.Drawing.Size(175, 24);
            this.shiftRegistersComboBox.TabIndex = 66;
            // 
            // outputPanelLabel
            // 
            this.outputPanelLabel.AutoSize = true;
            this.outputPanelLabel.Location = new System.Drawing.Point(19, 44);
            this.outputPanelLabel.Name = "outputPanelLabel";
            this.outputPanelLabel.Size = new System.Drawing.Size(132, 17);
            this.outputPanelLabel.TabIndex = 67;
            this.outputPanelLabel.Text = "Register Output Pin";
            // 
            // registerOutputPinComboBox
            // 
            this.registerOutputPinComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.registerOutputPinComboBox.FormattingEnabled = true;
            this.registerOutputPinComboBox.Location = new System.Drawing.Point(158, 41);
            this.registerOutputPinComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.registerOutputPinComboBox.MaximumSize = new System.Drawing.Size(177, 0);
            this.registerOutputPinComboBox.MinimumSize = new System.Drawing.Size(45, 0);
            this.registerOutputPinComboBox.Name = "registerOutputPinComboBox";
            this.registerOutputPinComboBox.Size = new System.Drawing.Size(136, 24);
            this.registerOutputPinComboBox.TabIndex = 68;
            this.registerOutputPinComboBox.SelectedIndexChanged += new System.EventHandler(this.ShiftRegisterSelectionChanged);
            // 
            // DisplayShiftRegisterPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.registerOutputPinComboBox);
            this.Controls.Add(this.outputPanelLabel);
            this.Controls.Add(this.shiftRegistersComboBox);
            this.Controls.Add(this.registerLabel);
            this.Name = "DisplayShiftRegisterPanel";
            this.Size = new System.Drawing.Size(323, 78);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label registerLabel;
        public System.Windows.Forms.ComboBox shiftRegistersComboBox;
        private System.Windows.Forms.Label outputPanelLabel;
        public System.Windows.Forms.ComboBox registerOutputPinComboBox;
    }
}
