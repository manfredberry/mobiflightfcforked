﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MobiFlight.UI.Panels
{
    public partial class DisplayShiftRegisterPanel : UserControl
    {

        public bool WideStyle = false;

        public DisplayShiftRegisterPanel()
        {
            InitializeComponent();
        }

        internal void SyncFromConfig(OutputConfigItem config)
        {
            // preselect display stuff
            if (config.ShiftRegister != null)
            {
                if (!ComboBoxHelper.SetSelectedItem(shiftRegistersComboBox, config.ShiftRegister.ToString()))
                {
                    // TODO: provide error message
                    Log.Instance.log("_syncConfigToForm : Exception on selecting item in Shift Register ComboBox", LogSeverity.Debug);
                }
            }

            if (config.RegisterOutputPin != null)
            {
                if (!ComboBoxHelper.SetSelectedItem(registerOutputPinComboBox, config.RegisterOutputPin.ToString()))
                {
                    // TODO: provide error message
                    Log.Instance.log("_syncConfigToForm : Exception on selecting item in Shift Register Output ComboBox", LogSeverity.Debug);
                }
            }
        }

        public void SetAddresses(List<ListItem> ports)
        {
            shiftRegistersComboBox.DataSource = new List<ListItem>(ports);
            shiftRegistersComboBox.DisplayMember = "Label";
            shiftRegistersComboBox.ValueMember = "Value";
            if (ports.Count > 0)
                shiftRegistersComboBox.SelectedIndex = 0;

            shiftRegistersComboBox.Enabled = ports.Count > 0;
            //shiftRegistersComboBox.Width = WideStyle ? shiftRegistersComboBox.MaximumSize.Width : shiftRegistersComboBox.MinimumSize.Width;
        }

        internal void SetVirtualPins(List<ListItem> virtualPins)
        {
            registerOutputPinComboBox.DataSource = new List<ListItem>(virtualPins);
            registerOutputPinComboBox.DisplayMember = "Label";
            registerOutputPinComboBox.ValueMember = "Value";
            if (virtualPins.Count > 0)
                registerOutputPinComboBox.SelectedIndex = 0;

            registerOutputPinComboBox.Enabled = virtualPins.Count > 0;

        }

        internal OutputConfigItem SyncToConfig(OutputConfigItem config)
        {
            config.ShiftRegister = shiftRegistersComboBox.SelectedValue as String;
            config.RegisterOutputPin = registerOutputPinComboBox.SelectedValue as String;

            return config;
        }

        private void ShiftRegisterSelectionChanged(object sender, EventArgs e)
        {

        }
      
    }
}
