﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MobiFlight.UI.Panels.Settings.Device
{
    public partial class MFAnalogPanel : UserControl
    {
        /// <summary>
        /// Gets raised whenever config object has changed
        /// </summary>
        public event EventHandler Changed;
        private MobiFlight.Config.AnalogDevice analog;
        bool initialized = false;

        public MFAnalogPanel()
        {
            InitializeComponent();
            mfPinComboBox.Items.Clear();
        }

        public MFAnalogPanel(MobiFlight.Config.AnalogDevice analogDevice, List<byte> FreePins)
            : this()
        {
            List<byte> Pin1Pins = FreePins.ToList(); Pin1Pins.Add(Convert.ToByte(analogDevice.Pin)); Pin1Pins.Sort();
            foreach (byte pin in Pin1Pins) mfPinComboBox.Items.Add(pin);

            if (mfPinComboBox.Items.Count > 0)
            {
                mfPinComboBox.SelectedIndex = 0;
            }

            // TODO: Complete member initialization
            this.analog = analogDevice;
            ComboBoxHelper.SetSelectedItem(mfPinComboBox, analog.Pin);
            textBox1.Text = analog.Name;
            setValues();

            initialized = true;
        }

        private void value_Changed(object sender, EventArgs e)
        {
            if (!initialized) return;

            setValues();

            if (Changed != null)
                Changed(analog, new EventArgs());
        }

        private void setValues()
        {
            analog.Pin = mfPinComboBox.Text;
            analog.Name = textBox1.Text;
        }
    }
}
