﻿
namespace MobiFlight.UI.Panels.Settings
{
    partial class MFShiftRegisterPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mfNumModulesComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.numberOfModulesLabel = new System.Windows.Forms.Label();
            this.mfPin1Label = new System.Windows.Forms.Label();
            this.mfPin1ComboBox = new System.Windows.Forms.ComboBox();
            this.mfPin3Label = new System.Windows.Forms.Label();
            this.mfPin3ComboBox = new System.Windows.Forms.ComboBox();
            this.mfPin2Label = new System.Windows.Forms.Label();
            this.mfPin2ComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mfNumModulesComboBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.numberOfModulesLabel);
            this.groupBox1.Controls.Add(this.mfPin1Label);
            this.groupBox1.Controls.Add(this.mfPin1ComboBox);
            this.groupBox1.Controls.Add(this.mfPin3Label);
            this.groupBox1.Controls.Add(this.mfPin3ComboBox);
            this.groupBox1.Controls.Add(this.mfPin2Label);
            this.groupBox1.Controls.Add(this.mfPin2ComboBox);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(313, 84);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pin and number of modules";
            // 
            // mfNumModulesComboBox
            // 
            this.mfNumModulesComboBox.FormattingEnabled = true;
            this.mfNumModulesComboBox.Location = new System.Drawing.Point(204, 47);
            this.mfNumModulesComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.mfNumModulesComboBox.Name = "mfNumModulesComboBox";
            this.mfNumModulesComboBox.Size = new System.Drawing.Size(49, 24);
            this.mfNumModulesComboBox.TabIndex = 21;
            this.mfNumModulesComboBox.SelectedIndexChanged += new System.EventHandler(this.value_Changed);
            this.mfNumModulesComboBox.SelectedValueChanged += new System.EventHandler(this.value_Changed);
            // 
            // label3
            // 
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(188, 50);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 14);
            this.label3.TabIndex = 20;
            this.label3.Text = "|";
            // 
            // numberOfModulesLabel
            // 
            this.numberOfModulesLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.numberOfModulesLabel.Location = new System.Drawing.Point(208, 26);
            this.numberOfModulesLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.numberOfModulesLabel.Name = "numberOfModulesLabel";
            this.numberOfModulesLabel.Size = new System.Drawing.Size(43, 14);
            this.numberOfModulesLabel.TabIndex = 19;
            this.numberOfModulesLabel.Text = "Num";
            // 
            // mfPin1Label
            // 
            this.mfPin1Label.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.mfPin1Label.Location = new System.Drawing.Point(8, 26);
            this.mfPin1Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.mfPin1Label.Name = "mfPin1Label";
            this.mfPin1Label.Size = new System.Drawing.Size(53, 17);
            this.mfPin1Label.TabIndex = 16;
            this.mfPin1Label.Text = "Latch";
            // 
            // mfPin1ComboBox
            // 
            this.mfPin1ComboBox.FormattingEnabled = true;
            this.mfPin1ComboBox.Location = new System.Drawing.Point(12, 47);
            this.mfPin1ComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.mfPin1ComboBox.Name = "mfPin1ComboBox";
            this.mfPin1ComboBox.Size = new System.Drawing.Size(49, 24);
            this.mfPin1ComboBox.TabIndex = 15;
            this.mfPin1ComboBox.SelectedIndexChanged += new System.EventHandler(this.value_Changed);
            this.mfPin1ComboBox.SelectedValueChanged += new System.EventHandler(this.value_Changed);
            // 
            // mfPin3Label
            // 
            this.mfPin3Label.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.mfPin3Label.Location = new System.Drawing.Point(125, 26);
            this.mfPin3Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.mfPin3Label.Name = "mfPin3Label";
            this.mfPin3Label.Size = new System.Drawing.Size(53, 14);
            this.mfPin3Label.TabIndex = 18;
            this.mfPin3Label.Text = "Data";
            // 
            // mfPin3ComboBox
            // 
            this.mfPin3ComboBox.FormattingEnabled = true;
            this.mfPin3ComboBox.Location = new System.Drawing.Point(129, 47);
            this.mfPin3ComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.mfPin3ComboBox.Name = "mfPin3ComboBox";
            this.mfPin3ComboBox.Size = new System.Drawing.Size(49, 24);
            this.mfPin3ComboBox.TabIndex = 17;
            this.mfPin3ComboBox.SelectedIndexChanged += new System.EventHandler(this.value_Changed);
            this.mfPin3ComboBox.SelectedValueChanged += new System.EventHandler(this.value_Changed);
            // 
            // mfPin2Label
            // 
            this.mfPin2Label.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.mfPin2Label.Location = new System.Drawing.Point(67, 26);
            this.mfPin2Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.mfPin2Label.Name = "mfPin2Label";
            this.mfPin2Label.Size = new System.Drawing.Size(55, 14);
            this.mfPin2Label.TabIndex = 16;
            this.mfPin2Label.Text = "Clock";
            // 
            // mfPin2ComboBox
            // 
            this.mfPin2ComboBox.FormattingEnabled = true;
            this.mfPin2ComboBox.Location = new System.Drawing.Point(71, 47);
            this.mfPin2ComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.mfPin2ComboBox.Name = "mfPin2ComboBox";
            this.mfPin2ComboBox.Size = new System.Drawing.Size(49, 24);
            this.mfPin2ComboBox.TabIndex = 15;
            this.mfPin2ComboBox.SelectedIndexChanged += new System.EventHandler(this.value_Changed);
            this.mfPin2ComboBox.SelectedValueChanged += new System.EventHandler(this.value_Changed);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 84);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(313, 59);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Name";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 29);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(241, 22);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextChanged += new System.EventHandler(this.value_Changed);
            // 
            // MFShiftRegisterPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "MFShiftRegisterPanel";
            this.Size = new System.Drawing.Size(313, 162);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox mfNumModulesComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label numberOfModulesLabel;
        private System.Windows.Forms.Label mfPin1Label;
        private System.Windows.Forms.ComboBox mfPin1ComboBox;
        private System.Windows.Forms.Label mfPin3Label;
        private System.Windows.Forms.ComboBox mfPin3ComboBox;
        private System.Windows.Forms.Label mfPin2Label;
        private System.Windows.Forms.ComboBox mfPin2ComboBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox1;
    }
}
