﻿namespace MobiFlight.UI.Panels.Settings
{
    partial class MobiFlightPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("LED Segment #1");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Servo #1");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Servo #2");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("MF_MEGA (COM3)", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3});
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("MF Micro (COM5)");
            this.mfConfiguredModulesGroupBox = new System.Windows.Forms.GroupBox();
            this.mfModulesTreeView = new System.Windows.Forms.TreeView();
            this.mfModuleSettingsContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ledOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ledSegmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.servoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stepperToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LcdDisplayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addShiftRegister74HC595ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.buttonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.encoderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.uploadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.updateFirmwareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regenerateSerialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reloadConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mfTreeViewImageList = new System.Windows.Forms.ImageList(this.components);
            this.mfSettingsPanel = new System.Windows.Forms.Panel();
            this.mobiflightSettingsToolStrip = new System.Windows.Forms.ToolStrip();
            this.uploadToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.addDeviceToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.addEncoderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addButtonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addAnalogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.addStepperToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addServoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addLedModuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addLcdDisplayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeDeviceToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.firmwareUpdateBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.mobiflightSettingsLabel = new System.Windows.Forms.Label();
            this.firmwareSettingsGroupBox = new System.Windows.Forms.GroupBox();
            this.FwAutoUpdateCheckBox = new System.Windows.Forms.CheckBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.shiftRegister74HC595ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mfConfiguredModulesGroupBox.SuspendLayout();
            this.mfModuleSettingsContextMenuStrip.SuspendLayout();
            this.mobiflightSettingsToolStrip.SuspendLayout();
            this.firmwareSettingsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // mfConfiguredModulesGroupBox
            // 
            this.mfConfiguredModulesGroupBox.Controls.Add(this.mfModulesTreeView);
            this.mfConfiguredModulesGroupBox.Controls.Add(this.mfSettingsPanel);
            this.mfConfiguredModulesGroupBox.Controls.Add(this.mobiflightSettingsToolStrip);
            this.mfConfiguredModulesGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mfConfiguredModulesGroupBox.Location = new System.Drawing.Point(0, 38);
            this.mfConfiguredModulesGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.mfConfiguredModulesGroupBox.Name = "mfConfiguredModulesGroupBox";
            this.mfConfiguredModulesGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.mfConfiguredModulesGroupBox.Size = new System.Drawing.Size(716, 486);
            this.mfConfiguredModulesGroupBox.TabIndex = 1;
            this.mfConfiguredModulesGroupBox.TabStop = false;
            this.mfConfiguredModulesGroupBox.Text = "Configured modules";
            // 
            // mfModulesTreeView
            // 
            this.mfModulesTreeView.ContextMenuStrip = this.mfModuleSettingsContextMenuStrip;
            this.mfModulesTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mfModulesTreeView.ImageIndex = 0;
            this.mfModulesTreeView.ImageList = this.mfTreeViewImageList;
            this.mfModulesTreeView.Location = new System.Drawing.Point(4, 19);
            this.mfModulesTreeView.Margin = new System.Windows.Forms.Padding(4);
            this.mfModulesTreeView.Name = "mfModulesTreeView";
            treeNode1.Name = "LED01";
            treeNode1.Text = "LED Segment #1";
            treeNode2.Name = "SERVO01";
            treeNode2.Text = "Servo #1";
            treeNode3.Name = "SERVO_02";
            treeNode3.Text = "Servo #2";
            treeNode4.Name = "Module_0";
            treeNode4.Text = "MF_MEGA (COM3)";
            treeNode5.Name = "MF_MICRO_COM5";
            treeNode5.Text = "MF Micro (COM5)";
            this.mfModulesTreeView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode4,
            treeNode5});
            this.mfModulesTreeView.SelectedImageIndex = 0;
            this.mfModulesTreeView.ShowNodeToolTips = true;
            this.mfModulesTreeView.Size = new System.Drawing.Size(447, 432);
            this.mfModulesTreeView.TabIndex = 0;
            this.mfModulesTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.mfModulesTreeView_AfterSelect);
            // 
            // mfModuleSettingsContextMenuStrip
            // 
            this.mfModuleSettingsContextMenuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mfModuleSettingsContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.removeToolStripMenuItem,
            this.toolStripMenuItem1,
            this.uploadToolStripMenuItem,
            this.toolStripMenuItem2,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripMenuItem3,
            this.updateFirmwareToolStripMenuItem,
            this.regenerateSerialToolStripMenuItem,
            this.reloadConfigToolStripMenuItem});
            this.mfModuleSettingsContextMenuStrip.Name = "mfModuleSettingsContextMenuStrip";
            this.mfModuleSettingsContextMenuStrip.Size = new System.Drawing.Size(211, 266);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ledOutputToolStripMenuItem,
            this.ledSegmentToolStripMenuItem,
            this.servoToolStripMenuItem,
            this.stepperToolStripMenuItem,
            this.LcdDisplayToolStripMenuItem,
            this.addShiftRegister74HC595ToolStripMenuItem,
            this.toolStripMenuItem4,
            this.buttonToolStripMenuItem,
            this.encoderToolStripMenuItem,
            this.analogToolStripMenuItem});
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.addToolStripMenuItem.Text = "Add device";
            // 
            // ledOutputToolStripMenuItem
            // 
            this.ledOutputToolStripMenuItem.Name = "ledOutputToolStripMenuItem";
            this.ledOutputToolStripMenuItem.Size = new System.Drawing.Size(254, 26);
            this.ledOutputToolStripMenuItem.Text = "LED / Output";
            this.ledOutputToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // ledSegmentToolStripMenuItem
            // 
            this.ledSegmentToolStripMenuItem.Name = "ledSegmentToolStripMenuItem";
            this.ledSegmentToolStripMenuItem.Size = new System.Drawing.Size(254, 26);
            this.ledSegmentToolStripMenuItem.Text = "LED 7-Segment";
            this.ledSegmentToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // servoToolStripMenuItem
            // 
            this.servoToolStripMenuItem.Name = "servoToolStripMenuItem";
            this.servoToolStripMenuItem.Size = new System.Drawing.Size(254, 26);
            this.servoToolStripMenuItem.Text = "Servo";
            this.servoToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // stepperToolStripMenuItem
            // 
            this.stepperToolStripMenuItem.Name = "stepperToolStripMenuItem";
            this.stepperToolStripMenuItem.Size = new System.Drawing.Size(254, 26);
            this.stepperToolStripMenuItem.Text = "Stepper";
            this.stepperToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // LcdDisplayToolStripMenuItem
            // 
            this.LcdDisplayToolStripMenuItem.Name = "LcdDisplayToolStripMenuItem";
            this.LcdDisplayToolStripMenuItem.Size = new System.Drawing.Size(254, 26);
            this.LcdDisplayToolStripMenuItem.Text = "LCD Display";
            this.LcdDisplayToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // addShiftRegister74HC595ToolStripMenuItem
            // 
            this.addShiftRegister74HC595ToolStripMenuItem.Name = "addShiftRegister74HC595ToolStripMenuItem";
            this.addShiftRegister74HC595ToolStripMenuItem.Size = new System.Drawing.Size(254, 26);
            this.addShiftRegister74HC595ToolStripMenuItem.Text = "Shift Register (74HC595)";
            this.addShiftRegister74HC595ToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(251, 6);
            // 
            // buttonToolStripMenuItem
            // 
            this.buttonToolStripMenuItem.Name = "buttonToolStripMenuItem";
            this.buttonToolStripMenuItem.Size = new System.Drawing.Size(254, 26);
            this.buttonToolStripMenuItem.Text = "Button";
            this.buttonToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // encoderToolStripMenuItem
            // 
            this.encoderToolStripMenuItem.Name = "encoderToolStripMenuItem";
            this.encoderToolStripMenuItem.Size = new System.Drawing.Size(254, 26);
            this.encoderToolStripMenuItem.Text = "Encoder";
            this.encoderToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // analogToolStripMenuItem
            // 
            this.analogToolStripMenuItem.Name = "analogToolStripMenuItem";
            this.analogToolStripMenuItem.Size = new System.Drawing.Size(254, 26);
            this.analogToolStripMenuItem.Text = "Analog Device";
            this.analogToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.removeToolStripMenuItem.Text = "Remove device";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeDeviceToolStripButton_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(207, 6);
            // 
            // uploadToolStripMenuItem
            // 
            this.uploadToolStripMenuItem.Name = "uploadToolStripMenuItem";
            this.uploadToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.uploadToolStripMenuItem.Text = "Upload Config";
            this.uploadToolStripMenuItem.Click += new System.EventHandler(this.uploadToolStripButton_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(207, 6);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripButton_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripButton_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.saveAsToolStripMenuItem.Text = "Save As...";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(207, 6);
            // 
            // updateFirmwareToolStripMenuItem
            // 
            this.updateFirmwareToolStripMenuItem.Name = "updateFirmwareToolStripMenuItem";
            this.updateFirmwareToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.updateFirmwareToolStripMenuItem.Text = "Update Firmware";
            this.updateFirmwareToolStripMenuItem.Click += new System.EventHandler(this.updateFirmwareToolStripMenuItem_Click);
            // 
            // regenerateSerialToolStripMenuItem
            // 
            this.regenerateSerialToolStripMenuItem.Name = "regenerateSerialToolStripMenuItem";
            this.regenerateSerialToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.regenerateSerialToolStripMenuItem.Text = "Regenerate Serial";
            this.regenerateSerialToolStripMenuItem.Click += new System.EventHandler(this.regenerateSerialToolStripMenuItem_Click);
            // 
            // reloadConfigToolStripMenuItem
            // 
            this.reloadConfigToolStripMenuItem.Name = "reloadConfigToolStripMenuItem";
            this.reloadConfigToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.reloadConfigToolStripMenuItem.Text = "Reload Config";
            this.reloadConfigToolStripMenuItem.Click += new System.EventHandler(this.reloadConfigToolStripMenuItem_Click);
            // 
            // mfTreeViewImageList
            // 
            this.mfTreeViewImageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.mfTreeViewImageList.ImageSize = new System.Drawing.Size(16, 16);
            this.mfTreeViewImageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // mfSettingsPanel
            // 
            this.mfSettingsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.mfSettingsPanel.Location = new System.Drawing.Point(451, 19);
            this.mfSettingsPanel.Margin = new System.Windows.Forms.Padding(4);
            this.mfSettingsPanel.Name = "mfSettingsPanel";
            this.mfSettingsPanel.Size = new System.Drawing.Size(261, 432);
            this.mfSettingsPanel.TabIndex = 1;
            // 
            // mobiflightSettingsToolStrip
            // 
            this.mobiflightSettingsToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.mobiflightSettingsToolStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mobiflightSettingsToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uploadToolStripButton,
            this.toolStripSeparator1,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator2,
            this.addDeviceToolStripDropDownButton,
            this.removeDeviceToolStripButton,
            this.toolStripSeparator4});
            this.mobiflightSettingsToolStrip.Location = new System.Drawing.Point(4, 451);
            this.mobiflightSettingsToolStrip.Name = "mobiflightSettingsToolStrip";
            this.mobiflightSettingsToolStrip.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.mobiflightSettingsToolStrip.Size = new System.Drawing.Size(708, 31);
            this.mobiflightSettingsToolStrip.TabIndex = 0;
            this.mobiflightSettingsToolStrip.Text = "toolStrip1";
            // 
            // uploadToolStripButton
            // 
            this.uploadToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.uploadToolStripButton.Image = global::MobiFlight.Properties.Resources.export1;
            this.uploadToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uploadToolStripButton.Name = "uploadToolStripButton";
            this.uploadToolStripButton.Size = new System.Drawing.Size(29, 28);
            this.uploadToolStripButton.Text = "Upload current config to module";
            this.uploadToolStripButton.Click += new System.EventHandler(this.uploadToolStripButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = global::MobiFlight.Properties.Resources.folder;
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(29, 28);
            this.openToolStripButton.Text = "Load config";
            this.openToolStripButton.Click += new System.EventHandler(this.openToolStripButton_Click);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = global::MobiFlight.Properties.Resources.disk_blue;
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(29, 28);
            this.saveToolStripButton.Text = "Save current config to file";
            this.saveToolStripButton.Click += new System.EventHandler(this.saveToolStripButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // addDeviceToolStripDropDownButton
            // 
            this.addDeviceToolStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addEncoderToolStripMenuItem,
            this.addButtonToolStripMenuItem,
            this.addAnalogToolStripMenuItem,
            this.toolStripSeparator3,
            this.addStepperToolStripMenuItem,
            this.addServoToolStripMenuItem,
            this.addLedModuleToolStripMenuItem,
            this.addOutputToolStripMenuItem,
            this.addLcdDisplayToolStripMenuItem});
            this.addDeviceToolStripDropDownButton.Image = global::MobiFlight.Properties.Resources.star_yellow_add;
            this.addDeviceToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addDeviceToolStripDropDownButton.Name = "addDeviceToolStripDropDownButton";
            this.addDeviceToolStripDropDownButton.Size = new System.Drawing.Size(122, 28);
            this.addDeviceToolStripDropDownButton.Text = "Add device";
            // 
            // addEncoderToolStripMenuItem
            // 
            this.addEncoderToolStripMenuItem.Name = "addEncoderToolStripMenuItem";
            this.addEncoderToolStripMenuItem.Size = new System.Drawing.Size(195, 26);
            this.addEncoderToolStripMenuItem.Text = "Encoder";
            this.addEncoderToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // addButtonToolStripMenuItem
            // 
            this.addButtonToolStripMenuItem.Name = "addButtonToolStripMenuItem";
            this.addButtonToolStripMenuItem.Size = new System.Drawing.Size(195, 26);
            this.addButtonToolStripMenuItem.Text = "Button";
            this.addButtonToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // addAnalogToolStripMenuItem
            // 
            this.addAnalogToolStripMenuItem.Name = "addAnalogToolStripMenuItem";
            this.addAnalogToolStripMenuItem.Size = new System.Drawing.Size(195, 26);
            this.addAnalogToolStripMenuItem.Text = "Analog";
            this.addAnalogToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(192, 6);
            // 
            // addStepperToolStripMenuItem
            // 
            this.addStepperToolStripMenuItem.Name = "addStepperToolStripMenuItem";
            this.addStepperToolStripMenuItem.Size = new System.Drawing.Size(195, 26);
            this.addStepperToolStripMenuItem.Text = "Stepper";
            this.addStepperToolStripMenuItem.Visible = false;
            this.addStepperToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // addServoToolStripMenuItem
            // 
            this.addServoToolStripMenuItem.Name = "addServoToolStripMenuItem";
            this.addServoToolStripMenuItem.Size = new System.Drawing.Size(195, 26);
            this.addServoToolStripMenuItem.Text = "Servo";
            this.addServoToolStripMenuItem.Visible = false;
            this.addServoToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // addLedModuleToolStripMenuItem
            // 
            this.addLedModuleToolStripMenuItem.Name = "addLedModuleToolStripMenuItem";
            this.addLedModuleToolStripMenuItem.Size = new System.Drawing.Size(195, 26);
            this.addLedModuleToolStripMenuItem.Text = "LED 7-Segment";
            this.addLedModuleToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // addOutputToolStripMenuItem
            // 
            this.addOutputToolStripMenuItem.Name = "addOutputToolStripMenuItem";
            this.addOutputToolStripMenuItem.Size = new System.Drawing.Size(195, 26);
            this.addOutputToolStripMenuItem.Text = "LED / Output";
            this.addOutputToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // addLcdDisplayToolStripMenuItem
            // 
            this.addLcdDisplayToolStripMenuItem.Name = "addLcdDisplayToolStripMenuItem";
            this.addLcdDisplayToolStripMenuItem.Size = new System.Drawing.Size(195, 26);
            this.addLcdDisplayToolStripMenuItem.Text = "LCD Display";
            this.addLcdDisplayToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // removeDeviceToolStripButton
            // 
            this.removeDeviceToolStripButton.Image = global::MobiFlight.Properties.Resources.star_yellow_delete;
            this.removeDeviceToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.removeDeviceToolStripButton.Name = "removeDeviceToolStripButton";
            this.removeDeviceToolStripButton.Size = new System.Drawing.Size(138, 28);
            this.removeDeviceToolStripButton.Text = "Remove device";
            this.removeDeviceToolStripButton.Click += new System.EventHandler(this.removeDeviceToolStripButton_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 31);
            // 
            // mobiflightSettingsLabel
            // 
            this.mobiflightSettingsLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.mobiflightSettingsLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.mobiflightSettingsLabel.Location = new System.Drawing.Point(0, 0);
            this.mobiflightSettingsLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.mobiflightSettingsLabel.Name = "mobiflightSettingsLabel";
            this.mobiflightSettingsLabel.Size = new System.Drawing.Size(716, 38);
            this.mobiflightSettingsLabel.TabIndex = 3;
            this.mobiflightSettingsLabel.Text = "Configure connected MobiFlight modules. You can save and load configurations and " +
    "upload them to your modules.";
            // 
            // firmwareSettingsGroupBox
            // 
            this.firmwareSettingsGroupBox.Controls.Add(this.FwAutoUpdateCheckBox);
            this.firmwareSettingsGroupBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.firmwareSettingsGroupBox.Location = new System.Drawing.Point(0, 524);
            this.firmwareSettingsGroupBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.firmwareSettingsGroupBox.Name = "firmwareSettingsGroupBox";
            this.firmwareSettingsGroupBox.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.firmwareSettingsGroupBox.Size = new System.Drawing.Size(716, 78);
            this.firmwareSettingsGroupBox.TabIndex = 4;
            this.firmwareSettingsGroupBox.TabStop = false;
            this.firmwareSettingsGroupBox.Text = "Firmware Flash Settings";
            // 
            // FwAutoUpdateCheckBox
            // 
            this.FwAutoUpdateCheckBox.AutoSize = true;
            this.FwAutoUpdateCheckBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.FwAutoUpdateCheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.FwAutoUpdateCheckBox.Location = new System.Drawing.Point(4, 18);
            this.FwAutoUpdateCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.FwAutoUpdateCheckBox.Name = "FwAutoUpdateCheckBox";
            this.FwAutoUpdateCheckBox.Padding = new System.Windows.Forms.Padding(13, 0, 0, 0);
            this.FwAutoUpdateCheckBox.Size = new System.Drawing.Size(708, 21);
            this.FwAutoUpdateCheckBox.TabIndex = 3;
            this.FwAutoUpdateCheckBox.Text = "Firmware Auto Upload enabled";
            this.FwAutoUpdateCheckBox.UseVisualStyleBackColor = true;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // shiftRegister74HC595ToolStripMenuItem
            // 
            this.shiftRegister74HC595ToolStripMenuItem.Name = "shiftRegister74HC595ToolStripMenuItem";
            this.shiftRegister74HC595ToolStripMenuItem.Size = new System.Drawing.Size(254, 26);
            this.shiftRegister74HC595ToolStripMenuItem.Text = "Shift Register (74HC595)";
            this.shiftRegister74HC595ToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // MobiFlightPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mfConfiguredModulesGroupBox);
            this.Controls.Add(this.firmwareSettingsGroupBox);
            this.Controls.Add(this.mobiflightSettingsLabel);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "MobiFlightPanel";
            this.Size = new System.Drawing.Size(716, 602);
            this.Load += new System.EventHandler(this.MobiFlightPanel_Load);
            this.mfConfiguredModulesGroupBox.ResumeLayout(false);
            this.mfConfiguredModulesGroupBox.PerformLayout();
            this.mfModuleSettingsContextMenuStrip.ResumeLayout(false);
            this.mobiflightSettingsToolStrip.ResumeLayout(false);
            this.mobiflightSettingsToolStrip.PerformLayout();
            this.firmwareSettingsGroupBox.ResumeLayout(false);
            this.firmwareSettingsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox mfConfiguredModulesGroupBox;
        private System.Windows.Forms.TreeView mfModulesTreeView;
        private System.Windows.Forms.Panel mfSettingsPanel;
        private System.Windows.Forms.ToolStrip mobiflightSettingsToolStrip;
        private System.Windows.Forms.ToolStripButton uploadToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripDropDownButton addDeviceToolStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem addEncoderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addButtonToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem addStepperToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addServoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addLedModuleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addLcdDisplayToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton removeDeviceToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.ComponentModel.BackgroundWorker firmwareUpdateBackgroundWorker;
        private System.Windows.Forms.ImageList mfTreeViewImageList;
        private System.Windows.Forms.ContextMenuStrip mfModuleSettingsContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ledOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ledSegmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem servoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stepperToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LcdDisplayToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem buttonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem encoderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem uploadToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem updateFirmwareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regenerateSerialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reloadConfigToolStripMenuItem;
        private System.Windows.Forms.Label mobiflightSettingsLabel;
        private System.Windows.Forms.GroupBox firmwareSettingsGroupBox;
        private System.Windows.Forms.CheckBox FwAutoUpdateCheckBox;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.ToolStripMenuItem shiftRegister74HC595ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addShiftRegister74HC595ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addAnalogToolStripMenuItem;
    }
}
