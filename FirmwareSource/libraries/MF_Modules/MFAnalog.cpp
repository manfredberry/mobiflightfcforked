// MFSegments.cpp
//
// Copyright (C) 2021

#include "MFAnalog.h"

MFAnalog::MFAnalog(uint8_t pin, const char * name)
{   
  _pin  = pin;
  _name = name;
  _lastValue = 0;
  _last = millis();
  _handler = callback;
  pinMode(_pin, INPUT_PULLUP);     // set pin to input. Could use OUTPUT for analog, but shows the intention :-)
  digitalRead(_pin); // turn on pullup resistors
}

void MFAnalog::update()
{
    uint32_t now = millis();
    if (now-_last <= 10) return;
    int newValue = (int) analogRead(_pin);
    _last = now;
    if (newValue!=_lastValue) {     
      _lastValue = newState;
       if (_handler!= NULL) {
        (*_handler)(_lastValue, _pin);
      }      
    }
}

void MFAnalog::attachHandler(buttonEvent callback)
{
  _handler = callback;
}

